#include "EquipmentIterator.h"


EquipmentIterator::EquipmentIterator(std::map<std::string, Item>* equipmentSlot) : _position(0) 
{
	for (auto entry : *equipmentSlot)
	{
		_items.push_back(entry.second);
	}
}

Item EquipmentIterator::next()
{
	return _items.at(_position++); 
}

bool EquipmentIterator::hasNext()
{
	if (_position >= _items.size)
		return false;

	return true;
}

void EquipmentIterator::remove()
{

}