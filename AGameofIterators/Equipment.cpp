#include "Equipment.h"
#include "EquipmentIterator.h"

#define HELM_SLOT "helm_slot"
#define ARMOR_SLOT "armor_slot"
#define GLOVES_SLOT "gloves_slot"
#define BOOTS_SLOT "boots_slot"
#define WEAPON_SLOT "weapon_slot"
#define RING_SLOT "ring_slot"

Equipment::Equipment() 
{
	_equipmentSlots = new std::map<std::string, Item>();

	addItem(HELM_SLOT,
		    "Designer Hood of Patterns",
		    "A hood that grants the wisdom of patterns!",
		    250,
		    true);

	addItem(ARMOR_SLOT,
			"Plate of The Iterator",
			"Protection against different damage type implementations!",
			150,
			true);

	addItem(WEAPON_SLOT,
		   "Sword of Refactor",
		   "Slashes trough legacy code like butter.",
		   500,
		   false);
}

Equipment::~Equipment() 
{
	delete _equipmentSlots;
}

void Equipment::addItem(const std::string& equipmentSlot,
						const std::string& name,
					    const std::string& description,
						const float& price,
						const bool& isSoulbound) 
{
	_equipmentSlots->emplace(equipmentSlot, Item(name, description, price, isSoulbound));
}

std::map<std::string, Item>* Equipment::getItems() 
{
	return _equipmentSlots;
}

ItemIterator* Equipment::createItemIterator()
{
	return new EquipmentIterator(_equipmentSlots);
}