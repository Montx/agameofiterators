#pragma once

#include "ItemIterator.h"
#include <vector>

class ItemShop 
{

public:
	ItemShop(std::vector<ItemIterator*>* _playerItems);

	void printAllItems();

private:
	std::vector<ItemIterator*>* _playerItems;

	void printItemsForIterator(ItemIterator* itemIterator);
};