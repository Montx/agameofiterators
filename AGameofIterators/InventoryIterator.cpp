#include "InventoryIterator.h"

InventoryIterator::InventoryIterator(Item* inventorySlots, int size) : _inventorySlots(inventorySlots), _size(size), _position(0) {}

Item InventoryIterator::next()
{
	return _inventorySlots[_position++];
}

bool InventoryIterator::hasNext()
{
	if (_position < _size && _inventorySlots[_position].getName != "Empty") 
	{
		return true;
	}

	return false;
}

void InventoryIterator::remove()
{

}