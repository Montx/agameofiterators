#pragma once

#include <string>

class Item {

public:
	Item() : _name("Empty"), _description("Empty"), _price(0), _isSoulbound(true) {};

	Item(const std::string& name, 
		 const std::string& description, 
		 const float& price,
		 const bool& isSoulbound);

	std::string getName();
	std::string getDescription();
	float getPrice();
	bool isSoulbound();

private:
	std::string _name;
	std::string _description;
	float _price;
	bool _isSoulbound;
};