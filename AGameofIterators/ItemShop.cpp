#include "ItemShop.h"
#include <iostream>

ItemShop::ItemShop(std::vector<ItemIterator*>* playerItems) : _playerItems(playerItems)
{}

void ItemShop::printAllItems() 
{
	for (int i = 0; i < _playerItems->size(); i++)
	{
		printItemsForIterator(_playerItems->at(i));
	}
}

void ItemShop::printItemsForIterator(ItemIterator* itemIterator)
{
	while (itemIterator->hasNext()) 
	{
		Item item = itemIterator->next();
		
		if (item.isSoulbound)
			continue;

		std::cout << "/nItem: " + item.getName() << std::endl;
		std::cout << "Description: " + item.getDescription() << std::endl;
		std::cout << "Price: " + std::to_string(item.getPrice()) << std::endl;
	}
}