#pragma once

#include "Item.h"
#include "ItemCollection.h"
#include <vector>

class Bank : ItemCollection
{
public:
	Bank();
	~Bank();

	void addItem(const std::string& name,
				 const std::string& description,
				 const float& price,
				 const bool& isSoulbound);

	std::vector<Item>* getItems();

	ItemIterator* createItemIterator();

private:
	std::vector<Item>* _bankSlots;
};