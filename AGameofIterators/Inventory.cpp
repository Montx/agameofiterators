#include "Inventory.h"
#include "InventoryIterator.h"

#include <iostream>

Inventory::Inventory() 
{
	_itemBag = new Item[MAX_INVENTORY_SIZE+1];

	addItem("Staff of Disintegration",
			"Upon being activated, it disintegrates. Single use.",
			10,
			false);

	addItem("Amulet of oppositional emotion",
			"Whatever the character's emotion is, its shown the opposite on his face. If he's sad, he'll smile, if he's fighting he'll giggle like a school girl, etc.",
			150,
			true);

	addItem("Glove of Clapping",
			"It's a single leather glove. When worn, a command word causes it to make a clapping noise",
			50,
			false);

	addItem("Sword of Juicy Cutting",
			"Sword that has the ability to cut only fruits",
			500,
			false);
}

Inventory::~Inventory() 
{
	delete[]_itemBag;
}

void Inventory::addItem(const std::string& name,
				        const std::string& description,
						const float& price,
						const bool& isSoulbound)
{
	if (_numberOfItems >= MAX_INVENTORY_SIZE)
	{
		std::cout << "Inventory is full!" << std::endl;
		return;
	}

	Item item = Item(name, description, price, isSoulbound);

	_itemBag[_numberOfItems++] = item;
}

Item* Inventory::getItems() 
{
	return _itemBag;
}

ItemIterator* Inventory::createItemIterator()
{
	return new InventoryIterator(_itemBag, MAX_INVENTORY_SIZE+1);
}