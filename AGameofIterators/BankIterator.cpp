#include "BankIterator.h"


BankIterator::BankIterator(std::vector<Item>* bankSlots) : _bankSlots(bankSlots), _position(0) {}

Item BankIterator::next()
{
	return _bankSlots->at(_position++);
}

bool BankIterator::hasNext()
{
	if (_position >= _bankSlots->size)
		return false;

	return true;
}

void BankIterator::remove()
{
	
}