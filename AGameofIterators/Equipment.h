#pragma once

#include "Item.h"
#include "ItemCollection.h"
#include <map>

class Equipment : public ItemCollection
{
	Equipment();
	~Equipment();

	void addItem(const std::string& equipmentSlot,
				 const std::string& name,
				 const std::string& description,
				 const float& price,
				 const bool& isSoulbound);

	std::map<std::string, Item>* getItems();

	ItemIterator* createItemIterator();

private:
	std::map<std::string, Item>* _equipmentSlots;
};