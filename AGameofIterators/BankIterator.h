#pragma once

#include "ItemIterator.h"
#include <vector>
#include <iterator>

class BankIterator : public ItemIterator
{
public:
	BankIterator(std::vector<Item>* bankSlots);

	Item next();

	bool hasNext();

	void remove();

private:
	std::vector<Item>* _bankSlots; 
	int _position;
};