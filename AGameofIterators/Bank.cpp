#include "Bank.h"
#include "BankIterator.h"

#include <iostream>

Bank::Bank()
{
	_bankSlots = new std::vector<Item>();

	addItem("Ring of Detect Fire",
		"If you are on fire, it will detected it!",
		100,
		false);

	addItem("Microsoft Wandows",
		"Cover your enemies vision in constant blue!",
		150,
		true);

	addItem("Golden Hammer",
		"A mighty hammer to solve all your problems in one smash!",
		500,
		false);

	addItem("Banana",
		"Just a normal banana.",
		1,
		false);

	addItem("Salt Potion",
		"For when you need more salt than you already have!",
		70,
		false);
}

Bank::~Bank() 
{
	delete _bankSlots;
}

void Bank::addItem(const std::string& name,
	const std::string& description,
	const float& price,
	const bool& isSoulbound)
{
	_bankSlots->push_back(Item(name, description, price, isSoulbound));
}

std::vector<Item>* Bank::getItems()
{
	return _bankSlots;
}

ItemIterator* Bank::createItemIterator()
{
	return new BankIterator(_bankSlots);
}