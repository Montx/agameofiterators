#include "Item.h"

Item::Item(const std::string& name,
		   const std::string& description,
		   const float& price,
		   const bool& isSoulbound) : _name(name),
								      _description(description),
									  _price(price),
									  _isSoulbound(isSoulbound) {}

std::string Item::getName()
{
	return _name;
}

std::string Item::getDescription()
{
	return _description;
}

float Item::getPrice()
{
	return _price;
}

bool Item::isSoulbound()
{
	return _isSoulbound;
}
