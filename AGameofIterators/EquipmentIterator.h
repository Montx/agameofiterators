#pragma once

#include "ItemIterator.h"
#include <map>
#include <vector>
#include <iterator>

class EquipmentIterator : public ItemIterator
{
public:
	EquipmentIterator(std::map<std::string, Item>* equipmentSlot);

	Item next();

	bool hasNext();

	void remove();

private:
	std::vector<Item> _items;
	int _position;
};