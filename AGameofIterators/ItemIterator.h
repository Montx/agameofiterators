#pragma once

#include "Item.h"

class ItemIterator 
{
public:
	virtual bool hasNext() = 0;
	virtual Item next() = 0;
	virtual void remove() = 0;
};