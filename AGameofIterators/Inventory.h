#pragma once

#include "Item.h"
#include "ItemCollection.h"

#define MAX_INVENTORY_SIZE 10

class Inventory : ItemCollection
{
public:
	Inventory();
	~Inventory();

	void addItem(const std::string& name, 
				 const std::string& description,
				 const float& price, 
				 const bool& isSoulbound);

	Item* getItems();

	ItemIterator* createItemIterator();

private:
	int _numberOfItems;
	Item* _itemBag;
};