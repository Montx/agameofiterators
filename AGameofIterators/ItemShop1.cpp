#include "ItemShop1.h"
#include <iostream>

ItemShop::ItemShop(Inventory* inventory, Bank* bank) : _inventory(inventory), _bank(bank) {}

void ItemShop::printAllItems()
{
	Item* inventoryItems = _inventory->getItems();

	for (int i = 0; i < MAX_INVENTORY_SIZE + 1; i++)
	{
		printItem(inventoryItems[i]);
	}

	std::vector<Item>* bankItems = _bank->getItems();

	for (Item item : *bankItems) 
	{
		printItem(item);
	}
}

void ItemShop::printItem(Item item) 
{
	if (item.isSoulbound)
		return;

	std::cout << "/nItem: " + item.getName() << std::endl;
	std::cout << "Description: " + item.getDescription() << std::endl;
	std::cout << "Price: " + std::to_string(item.getPrice()) << std::endl;
}