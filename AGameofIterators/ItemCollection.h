#pragma once

#include "ItemIterator.h"

class ItemCollection
{
	virtual ItemIterator* createItemIterator() = 0;
};