#pragma once

#include "ItemIterator.h"

class InventoryIterator : public ItemIterator 
{
public:
	InventoryIterator(Item* inventorySlots, int size);

	Item next();

	bool hasNext();

	void remove();

private:
	Item* _inventorySlots;
	int _position;
	int _size;
};