#pragma once

#include "Inventory.h"
#include "Bank.h"

class ItemShop
{

public:
	ItemShop(Inventory* inventory, Bank* bank);

	void printAllItems();

private:
	Inventory* _inventory;
	Bank* _bank;

	void printItem(Item item);
};